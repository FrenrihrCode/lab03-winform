﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Data.SqlClient;

namespace Lab03
{
    public partial class frmLogin : Form
    {
        SqlConnection conn;
        public frmLogin(SqlConnection conn)
        {
            this.conn = conn;
            InitializeComponent();
        }
        //Esto permitirá mover el formulario :D
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int lparam);

        //Funcion para ingresar al sistema
        private void btnIngresar_Click(object sender, EventArgs e)
        {
            string userText = txtUsername.Text;
            string passText = txtContraseña.Text;
            //Verificacion simple por si el usuario no ha escrito sus datos
            if (userText != "" && passText != "")
            {
                //un try catch en caso de algun error inesperado
                try
                {
                    //verificar si la conexion esta abierta
                    // esto puede dar un error inesperado
                    if (conn.State == ConnectionState.Open)
                    {
                        //comenzamos la consulta
                        string sql =
                            "SELECT * FROM tbl_usuario " +
                            "WHERE usuario_nombre=@user AND usuario_password=@pass";
                        SqlCommand cmd = new SqlCommand();
                        cmd.Connection = conn;
                        //usaremos parametros para pasarle algunos valores a la query
                        cmd.Parameters.AddWithValue("@user", userText);
                        cmd.Parameters.AddWithValue("@pass", passText);
                        cmd.CommandText = sql;
                        cmd.CommandType = CommandType.Text;
                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                lblError.Text = "";
                                MessageBox.Show($"Bienvenido {reader.GetString(1)}");
                            }
                        }
                        else
                        {
                            lblError.Text = "Credenciales incorrectas";
                        }
                        reader.Close();
                    }
                    else
                        lblError.Text = "La conexión esta cerrada";
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Ocurrió un error:\n" + ex.ToString());
                }
            } else
            {
                lblError.Text = "Rellene sus datos por favor";
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnMinimize_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void titleBar_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(Handle, 0x112, 0xf012, 0);
        }

        private void linkTwitter_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            // Specify that the link was visited.
            this.linkTwitter.LinkVisited = true;
            // Navigate to a URL.
            System.Diagnostics.Process.Start("https://twitter.com/frenrihr_code");
        }

        private void btnIngresar_Paint(object sender, PaintEventArgs e)
        {
            //Rounded rectangle corder radius. The radius must be less than 10.
            System.Drawing.Drawing2D.GraphicsPath path = new System.Drawing.Drawing2D.GraphicsPath();// Create a line
            Rectangle myRectangle = btnIngresar.ClientRectangle;
            myRectangle.Inflate(0, 20);
            path.AddEllipse(myRectangle);
            btnIngresar.Region = new Region(path);
        }

        private void frmLogin_Load(object sender, EventArgs e)
        {
            lblError.Text = "";
        }
    }
}
